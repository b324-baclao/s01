<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control Structure</title>
	</head>
	<body>
		<h1>Echoing Values</h1>

		<!-- using single quote for the echo -->
		<p><?php echo 'Good day $name! Your given email is $email.'; ?></p>

		<!-- using double quoute for the echo -->
		<p><?php echo "Good day, $name! Your given email is $email."; ?></p>

		<p><?php echo PI; ?></p>
		<p><?php echo $name; ?></p>

		<h1>Data Types</h1>
		<h3>String</h3>
		<p><?php echo $state; ?></p>
		<p><?php echo $country; ?></p>
		<p><?php echo $address; ?></p>

		<h3>Interger</h3>
		<p><?php echo $age; ?></p>
		<p><?php echo $headcount; ?></p>

		<h3>Floats</h3>
		<p><?php echo $grade; ?></p>
		<p><?php echo $distanceInKilometers; ?></p>


		<h3>Boolean</h3>
		<!-- Normal echoing of boolean variables will not make it visible to the web page. -->
		<!-- The var_dump() function is handy during development to understand the data stored in variables. It helps you examine the data type and the actual value of a variable. The output of var_dump() may look a bit technical, but it provides valuable insights when you need to debug or understand what's inside a variable. -->
		<p><?php echo var_dump($hasTraveledAbroad); ?></p>
		<p><?php echo var_dump($haveSymptoms); ?></p>

		<h3>Null</h3>
		<p><?php echo var_dump($spouse); ?></p>
		<p><?php echo var_dump($middle); ?></p>

		<h3>Array</h3>
		<p><?php echo $grades[0]; ?></p>

		<h3>Objects</h3>
		<!--  -->
		<p><?php  echo $gradesObj->firstGrading; ?></p>
		<p><?php  echo $personObj->address->state; ?></p>
		<p><?php echo $personObj->contact[0]; ?></p>

		<h3>gettype</h3>
		<p>state: <?php echo gettype($state); ?></p> 
		<p>age: <?php echo gettype($age); ?></p>
		<p>grade: <?php echo gettype($grade); ?></p>
		<p>hasTraveledAbroad: <?php echo gettype($hasTraveledAbroad); ?></p>
		<p>haveSymptoms: <?php echo gettype($haveSymptoms); ?></p>
		<p>spouse: <?php echo gettype($spouse); ?></p>
		<p>grades: <?php echo gettype($grades); ?></p>
		<p>gradesObj: <?php echo gettype($gradesObj); ?></p>

		<h1>Operators</h1>
		<h3>Assignment Operator</h3>
		<p>X: <?php echo $x; ?></p>
		<p>Y: <?php echo $y; ?></p>

		<p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
		<p>Is Registered: <?php echo var_dump($isRegistered); ?></p>


		<h3>Arithmetic Operators</h3>
		<p>Sum: <?php echo $x + $y; ?></p>
		<p>Difference: <?php echo $x - $y; ?></p>
		<p>Product: <?php echo $x * $y; ?></p>
		<p>Quotient: <?php echo $x / $y; ?></p>


		<h3>Equality Operators</h3>

		<!-- Strict equality/inequality is preferred so that we an check both of the value and the data type given. -->

		<p>Loose Equality: <?php var_dump($x == '1234.14'); ?></p>
		<p>Strict Equality: <?php var_dump($x === '1234.14'); ?></p>

		<h3>Inequality Operators</h3>
		<p>Loose Inequality: <?php echo var_dump($x != '1234.14'); ?></p>
		<p>Strict Inequality: <?php var_dump($x !== '1234.14'); ?></p>

		<h3>Greater/Lesser Operator</h3>
		<p>Is Lesser: <?php echo var_dump($x < $y);  ?></p>
		<p>Is Greater: <?php echo var_dump($x > $y);  ?></p>

		<p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
		<p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>

		<h3>Logical Operators</h3>
		<!-- Logical Operati=ors are used to verify whether an expression or group of expression are either true or false -->
		
		<p>OR operator: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
		<p>AND operator: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
		<p>NOT operator: <?php echo var_dump(!$isLegalAge); ?></p>


		<h1>Function</h1>
		<p><?php echo getFullName('John', 'D.', 'Smith'); ?></p>


		<h1>Selection Control Structures</h1>

		<h3>If - Else If - Else Statement</h3>
		<p><?php echo determineTyphoonIntensity(39); ?></p>


		<h2>Ternary SAmple (Is Underage?)</h2>
		<p>78: <?php echo var_dump(isUnderAge(78)) ?></p>
		<p>17: <?php echo var_dump(isUnderAge(17)) ?></p>

		<h2>Switch</h2>
		<p><?php echo determineComputerUser(5) ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php echo greeting('hello'); ?></p>



	</body>
</html>
