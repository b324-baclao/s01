<?php 


// $trial = "Hello World";
// [SECTION] - Comments
// Comments are part of the code that gets ignored by the language.
// Commnets are meant to describe the algorithm of the written code.

/*
	There are two types of comments:
	- The single-line comment denoted by two forward slashes (//)
	- The multi-line comment denoted by a slash and asterisk (/*)
*/

// [SECTION] - Variables
// Variables are used to contaion data.
// Variables are named location for the stored value.
// Variables are defined using the dollar ($) notation before the name of the variable

$name = 'John Smith';
$email = 'johnsmith@gmail.com';


// [SECTION] - Constants
// Constants used to hold data that are meant to be read-only.
// Constants are defined using the define() function.


/*
	Syntax:
		define ('variableName', value0fTheVariable);
*/


define('PI', 3.1416);

//Reassignment of a variable $name
$name = 'Will Smith';


// [SECTION] - Data Types in PHP

// Strings

$state = 'New York';
$country = 'United States of America';

// Concatenation using dot (.)
$address = $state . ', ' . $country;

// String interpolation (variable substitution) with double quotes
$address = "$state, $country.";

// Integers - Whole Numbers
$age = 31;
$headcount = 26;

// Floats - With Decimal Value
$grade = 98.2;
$distanceInKilometers = 48.6;

// Boolean
$hasTraveledAbroad = false;
$haveSymptoms = true;


// Null
$spouse = null;
$middle = null;


// Array
$grades = array(98.7, 92.1, 90.2, 94.6);

// Objects
$gradesObj = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object) [
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => 'United States of America'
	],
	'contact' => array('091234456', '0912112121')

];

// [SECTION] Operators
// Assignment Operator (=) 

// This operator is used to assign and reassign value/s of a variable
$x = 1234.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;


// [SECTION] Functions
// Functions are used to make reusable codes

function getFullName($firstName, $middleInitial, $lastName) {
	// You can add here your algorithm

	return "$lastName, $firstName $middleInitial";
}


// [SECTION] Selection Control Structures
// election Control Structures are used to make code execution dynamic according to predefined conditions
	
	/*
		- if
		- else if
		- else
	*/

function determineTyphoonIntensity($windSpeed) {

	if($windSpeed < 30) {
		return 'Not a typhoon yet.';
	} else if($windSpeed <= 61) {
		return 'Tropical Depression Detected.';
	} else if($windSpeed <= 88) {
		return 'Tropical Storm Detected.';
	} else if($windSpeed <= 117) {
		return 'Severe Tropical Storm Detected.';
	} else {
		return 'Typhoon Detected.';
	}
}

// Conditional (Ternary) Operator
function isUnderAge($age) {
	return ($age < 18) ? true : false;
}


// Switch Statement
function determineComputerUser($computerNumber) {
    switch ($computerNumber) {
        case 1:
            return 'Linus Torvalds';
            break;
        case 2:
            return 'Steve Jobs';
            break;
        case 3:
            return 'Sid Meir';
            break;
        case 4: 
            return 'Onel de Guzman';
            break;
        case 5:
            return 'Christian Salvador';
            break;
        // Default case: If $computerNumber does not match any of the cases (1, 2, 3, 4, or 5)
        default:
            return $computerNumber . ' is out of bounds.';
    }
}

// Try-Catch-Finally Statement
function greeting($str) {
    try {
        // Check if $str is a string
        if (gettype($str) == "string") {
            // If it is a string, echo the value
            echo $str;
        } else {
            // If it is not a string, throw a new Exception with the message "Oops!"
            throw new Exception("Oops!");
        }
    } catch (Exception $e) {
        // Catch the Exception and echo its error message using getMessage() method
        echo $e->getMessage();
    } finally {
        // The code inside 'finally' block always executes, regardless of whether an exception was caught or not.
        echo "I did it again!";
    }
}





?>



























